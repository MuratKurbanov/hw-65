import React from 'react';
import {NavLink} from "react-router-dom";

const NavigationPage = ({to, exact, children}) => {
    return (
        <div className='NavigationPage'>
            <NavLink to={to} exact>
                {children}
            </NavLink>
        </div>
    );
};

export default NavigationPage;