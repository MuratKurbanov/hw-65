import React, { Component } from 'react';
import './App.css';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router";
import EditPage from "./components/EditPage/EditPage";
import InfoPage from "./container/InfoPage/InfoPage";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/pages/:name/edit" exact component={EditPage}/>
          <Route path="/pages/:name" component={InfoPage}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
