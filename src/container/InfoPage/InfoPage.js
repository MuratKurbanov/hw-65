import React, {Component} from 'react';
import axios from "../../axios-page";


class InfoPage extends Component {

    state = {
        title: '',
        content: ''
    };

    pageData() {
        const name = this.props.match.params.name;
        axios.get('pages/' + name + '.json').then(response => {
            console.log(response.data);
            this.setState({title: response.data.title, content: response.data.content})
        });
    }

    componentDidMount() {
        this.pageData()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.match.params.name !== prevProps.match.params.name) {
            this.pageData()
        }
    }

    render() {
        return (
            <div className='info'>
                <h1>{this.state.title}</h1>
                <p>{this.state.content}</p>
            </div>
        );
    }
}

export default InfoPage;